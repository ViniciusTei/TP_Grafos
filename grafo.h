#ifndef GRAFO_H_INCLUDED
#define GRAFO_H_INCLUDED

#include <stdio.h>

#define false 0
#define true 1
#define qtd 1000

/* Armazena os dados da aresta
 * linha representa o primeiro vertice
 * e coluna o segundo. Usada para ler o
 * arquivo de entrada.
 */
typedef struct{
    int linha;
    int coluna;
    float peso;
}Aresta;

/* Armazena a matriz de valores do grafo
 */
typedef struct{
    int numeroVertices;
    int numeroArestas;
    float **matriz;
    float **matrizAuxiliar;
}Grafo;

int contaProfundidade;
int preProfundidade[qtd];
int retornoProfundidade[qtd][2];

int *largura;
int inicioLargura;
int fimLargura;
int lowProfundidade[qtd];

int corBipartido[qtd];
typedef struct no *link;

struct no{
    int w;
    link proximo;
};

typedef struct item{
    int grau;
    int index;
    int marcado;
}Item;

int *rotCaminhoMinimo;
float *distanciaCaminhoMinimo;
int *vetCaminhoMinimo;

//int parentGeradorMinima[qtd];

int numeroPontes;
//int lbl[qtd];

int verticePonte1;
int verticePonte2;


void inicializarMatriz(TipoGrafo *mat);
void imprimir(Grafo grafo);
float **alocarMatriz(int linhas);
int grau(Grafo grafo, int vertice);
void vizinhos(Grafo grafo, int vertice);
int verificarEuleriano(Grafo *grafo);

//BUSCA EM PROFUNDIDADE

void buscaProfundidadeR(Grafo grafo, int v);
void buscaProfundidade(Grafo grafo);
void imprimirProfundidade(Grafo grafo);

//BUSCA EM LARGURA

int filaVazia();
void fazFila(int maxVertices);
void enfileirar(Item item);
Item desinfileirar();
void liberarFila();
void buscaLargura(Grafo grafo, int s);

//GRAFO BIPARTIDO

int grafoHTWColor(Grafo grafo);
int dfsRColor(Grafo grafo, int vertice, int c);

//CAMINHO MINIMO

void grafoDijkstra(Grafo grafo, int s);
void inicializarVariaveis(Grafo grafo, int s);

//ARVORE GERADORA MINIMA

void grafoMST(Grafo grafo, int parent[]);

//COMPOMENTE CONEXA

int grafoComponenteConexa(Grafo grafo);
void dfsRComponenteConexa(Grafo grafo, int vertice, int id);

//PONTE

int grafoPonteDeBridges(Grafo grafo);
void ponteBridge(Grafo grafo, int vertice);

//CIRCUITO NEGATIVO

float acharMenorDistancia(float **matriz, float tamanhoMatriz, float verticeinicial, float verticefinal);
float pontoMinimo(float x, float y);
void imprimirMatrizCircuitoNegativo(float **matriz, float tamanhoMatriz);
void Floyd(Grafo grafo, float verticeinicial, float verticefinal);
void greedy(Grafo grafo);
void ordenarGrafo(Item* item, int n);
//euleriano
void grafoEuleriano(Grafo grafo);
void pegarCiclosNegativos(int *ciclo, int *subciclo, int posicao);
int possuiArestas(Grafo grafo, int tamanho);
void eliminarArestas(Grafo grafo, int *ciclo);
int *encontrarCiclo(Grafo grafo, int vertice, int n);
void mostrarCiclo(int *ciclo);
int dentroCiclo(int *ciclo, int vertice);
//void excluirMatrizAdjacencia(int n);


#endif // GRAFO_H_INCLUDED
