#include "grafo.h"

void inicializarMatriz(Grafo *mat)//vinicius
{
    mat->matriz = NULL;
    mat->numeroArestas = 0;
    mat->numeroVertices = 0;
}
void imprimir(Grafo grafo)//vinicius
{
    int i, j;

    for(i = 0; i < grafo.numVertices; i++)
    {
        for(j = 0; j < grafo.numVertices; j++)
        {
            printf("%.1lf ", grafo.matriz[i][j]);
        }
        printf("\n");
    }

}
float **alocarMatriz(int linhas)//vinicius
{
    int i,j, colunas;
    colunas = linhas;
    float **vertice;

    vertice = (float **)malloc((linhas)*sizeof(float*));
    if(vertice == NULL)
    {
        return NULL;
    }

    for(i =0; i<linhas; i++)
    {
        vertice[i] = (float*) malloc((colunas)*sizeof(float));
        if(vertice[i] == NULL)
        {
            return NULL;
        }
        for(i=0; i< linhas; i++)
        {
            for(j=0; j<colunas; j++)
            {
                vertice[i][j] = 0;
            }
        }
        return vertice;
    }
int grau(Grafo grafo, int vertice//athena
    {
        int i,j,grau=0;
        for(i=0; i<grafo.numerovertices; i++)
        {
            if(grafo.matriz[vertice-1][i]!=0)
            {
                grau++;
            }
            return grau;
        }
    }
void vizinhos(Grafo grafo, int vertice)
    {
    }
int verificarEuleriano(Grafo *grafo)
    {
    }

//BUSCA EM PROFUNDIDADE

void buscaProfundidadeR(Grafo grafo, int v)
    {
    }
void buscaProfundidade(Grafo grafo)
    {
    }
void imprimirProfundidade(Grafo grafo)
    {
    }

//BUSCA EM LARGURA

int filaVazia()
    {
    }
void fazFila(int maxVertices)
    {
    }
void enfileirar(Item item)
    {
    }
Item desinfileirar()
    {
    }
void liberarFila()
    {
    }
void buscaLargura(Grafo grafo, int s)
    {
    }

//GRAFO BIPARTIDO

int grafoHTWColor(Grafo grafo)
    {
    }
int dfsRColor(Grafo grafo, int vertice, int c)
    {
    }

//CAMINHO MINIMO

void grafoDijkstra(Grafo grafo, int s)
    {
    }
void inicializarVariaveis(Grafo grafo, int s)
    {
    }

//ARVORE GERADORA MINIMA

void grafoMST(Grafo grafo, int parent[])
    {
    }

//COMPOMENTE CONEXA

int grafoComponenteConexa(Grafo grafo)
    {
    }
void dfsRComponenteConexa(Grafo grafo, int vertice, int id)
    {
    }

//PONTE

int grafoPonteDeBridges(Grafo grafo)
    {
    }
void ponteBridge(Grafo grafo, int vertice)
    {
    }

//CIRCUITO NEGATIVO

float acharMenorDistancia(float **matriz, float tamanhoMatriz, float verticeinicial, float verticefinal)//athena
    {
        int i,j,k;
        for(i=0; i<tamanhoMatriz; ++i)
        {
            for(j=0; j<tamanhoMatriz; ++j)
            {
                if(j!=i && matriz[j][i]<999)
                {
                    for(k=0; k<tamanhoMatriz; ++k)
                    {
                        matriz[j][k] = pontoMinimo(matriz[j][k], matriz[j][i] + matriz[i][k]);

                    }
                }
            }
        }
        imprimirMatrizCircuitoNegativo(matriz,tamanhoMatriz);
        return matriz[verticeinicial-1][verticefinal-1];

    }
float pontoMinimo(float x, float y)//athena
    {
        if(x<y)
        {
            return x;
        }
        else
        {
            return y;
        }
    }
void imprimirMatrizCircuitoNegativo(float **matriz, float tamanhoMatriz)//athena
    {
        int i,j;
        for(i=0; i<tamanhoMatriz; ++i)
        {
            for(j=0; j<tamanhoMatriz; ++j)
            {
                printf("%.2f\t",matriz[i][j]);
            }
            printf("\n");
        }
    }
void Floyd(Grafo grafo, float verticeinicial, float verticefinal)//athena//Calcula o menor caminho entre todos os pares de vértices do grafo.
    {
        int numerolinhas,vertices;
        int i,j;
        int status = 0;
        int distancia;
        float **matriz, menordistancia;
        vertices = grafo.numerovertices;
        matriz = (float***)malloc(grafo.numerovertices * sizeof(float*));
        for(i=0; i<grafo.numerovertices; i++)
        {
            matriz[i] = (float*)malloc(grafo.numerovertices * sizeof(float));
            for(j=0; j<grafo.numerovertices; j++)
            {
                matriz[i][j] = 9999;//pq a quantidade de vertices é 1000
            }
        }
        for(i=0; i<grafo.numerovertices; i++)
        {
            for(j=0; j<grafo.numerovertices; j++)
            {
                if(grafo.matri[i][j]!= 0)
                {
                    matriz[i][j] = grafo.matriz[i][j];
                    matriz[j][i] = grafo.matriz[i][j];
                }
            }
        }
        menordistancia = acharMenorDistancia(matriz,vertices,verticeinicial,verticefinal);
        printf("Os vertices sao : %.2f %.2f\n",verticeinicial,verticefinal );
        printf("A menor distancia entre eles é: \n", menordistancia);
    }
void greedy(Grafo grafo)//arvore geradora minina//athena
    {
        int k,aux,i,j,n,alpa=0;
        int vetS[grafo.numerovertices];
        int conjuntoN = grafo.numerovertices;

        Item vertices[grafo.numerovertices];
        n=grafo.numerovertices;

        for(j=0; j<grafo.numerovertices; j++)
        {
            vertices[j].grau=0;
            vertices[j].index =0;
            vertices[j].marca=-1;
            vetS[j]=-1;
        }
        for(i=0; i<grafo.numerovertices; i++)
        {
            for(j=0; j<grafo.numerovertices; j++)
            {
                if(grafo.matriz[i][j] != 0)
                {
                    vertices[i].grau = vertices[i].grau++;
                    vertices[i].index=i;
                }
            }
        }
        ordenarGrafo(vertices,n);
        aux=0;
        for(k=0; k<grafo.numerovertices; k++)
        {
            printf("Valor de n %d\n",n);
            if(vertices[aux].marcado == -1)
            {
                vetS[vertices[aux].index] = aux;
                vertices[aux].marca = 0;
                for(i = 0; grafo.numerovertices; i++)
                {
                    if(grafo.matriz[aux][i]!= 0)
                    {
                        vertices[i].marcado = i;
                        printf("Vizinho %d\n",i);
                        n--;
                    }
                }
                alfa++;
            }
            aux++;
        }
        for(i=0; i<grafo.numerodevertices; i++)
        {
            if(vetS[i]!=-1)
            {
                printf("Item do conjunto %d\n", vetS[i]+1);
            }
        }
        printf("Alfa %d\n", alfa);
        return;
    }
void ordenarGrafo(Item* item, int n)//athena
    {
        int i,j;
        int h=1;
        Item aux;

        do
            h = h * 3 + 1;
        while (h <n);
        do
        {
            h = h/3;
            for(i=h; i<n; i++)
            {
                aux = item[i];
                j = i;
                while(item[j-h].grau < aux.grau)
                {
                    item[j] = item[j-h];
                    j-= h;
                    if(j <h)
                    {
                        break;
                    }
                }
                item[j] = aux;
            }
        }
        while(h!=1);
        return;
    }
void grafoEuleriano(Grafo grafo)//athena
    {
        int i,j;
        for(i=1; i < grafo->numeroVertices; i++)
        {
            if(grau(*grafo, i)%2 != 0)   //grau impar nao pode ser euleriano
            {
                return 1;
            }
        }
        return 0;
    }
void pegarCiclosNegativos(int *ciclo, int *subciclo, int posicao)//athena
    {
        int i, tamanhociclo, tamanhosubciclo;

        for(i = 0; ciclo[i] != -1; i++)
        {
            tamanhociclo=i;
        }
        for(i = 0; subciclo[i] != -1; i++)
        {
            tamanhosubciclo=i;
        }
        int *cicloaux = malloc(sizeof(int[tamanhociclo+ tamanhosubciclo + 1]));

        for(i=0; i< posicao; i++)
        {
            cicloaux[i] = ciclo[i];
        }
        for(i=posicao; i< posicao + tamanhosubciclo; i++)
        {
            cicloaux[i] = subciclo[i - posicao];
        }
        for(i=posicao + tamanhociclo + tamanhosubciclo; i< tamanhociclo + tamanhosubciclo; i++)
        {
            cicloaux[i] = ciclo[i - tamanhosubciclo];
        }
        for(i=0; i< tamanhociclo + tamanhosubciclo; i++)
        {
            ciclo[i] = cicloaux[i];
        }
        free(cicloaux);

    }
int possuiArestas(Grafo grafo, int tamanho)//athena
    {
        int i,j;
        for(i=0; i<tamanho; i++)
        {
            for(j=0; j<tamanho; j++)
            {
                if(grafo.matrizAuxiliar[i][j] == 1)
                {
                    return true;
                }
            }
        }
        return false;
    }
void eliminarArestas(Grafo grafo, int *ciclo)//athena
    {
        int i;
        if(ciclo[0] == -1) //fim do ciclo
        {
            return;
        }
        i = 1;
        while(ciclo[i] != -1)
        {
            grafo.matrizAuxiliar[ciclo[i-1]][ciclo[i]] = 0;
            grafo.matrizAuxiliar[ciclo[i]][ciclo[i-1]] = 0;
            i++;
        }
        grafo.matrizAuxiliar[ciclo[i-1]][ciclo[0]] = 0;
        grafo.matrizAuxiliar[ciclo[0]][ciclo[i-1]] = 0;
    }
int *encontrarCiclo(Grafo grafo, int vertice, int n)//athena
    {
        int vert,i,j,k;
        int *ciclo = malloc(sizeof(int[n*n + 1]));
        ciclo[0] = -1;
        vert = vertice;
        i=0;
        do
        {
            int j=0;
            while(grafo.matrizAuxiliar[vert][j] == 0 || dentroCiclo(ciclo, j))
            {
                j++
                while(j == n)
                {
                    i--;
                    if(i < 0)
                    {
                        return NULL;
                    }
                    else if(i == 0 )
                    {
                        vert = vertice;
                    }
                    else
                    {
                        v = ciclo[i-1];
                    }
                    j = ciclo[i] + 1;
                    ciclo[i] = -1;
                }
            }

            vert = j;
            ciclo[i] = vert;
            ciclo[i+1] = -1;
            i++

            if(i >= n*n)
            {
                return NULL;
            }

        }
        while(vert ! = vertice || i <=2);

        return ciclo;

    }
void mostrarCiclo(int *ciclo)//athena
    {
        int i;

        for(i=0; ciclo[i]!=-1; i++)
        {
            printf("Ciclo : %d", ciclo[i]+1);
        }
        printf("\n");
    }
int dentroCiclo(int *ciclo, int vertice)//athena
    {
        int naociclo = false;
        int k = 0;

        while(ciclo[k] != -1 && ! naociclo)
        {
            k++;
        }
        return naociclo;
    }
    /*void excluirMatrizAdjacencia(int n)//athena
    {

    }
